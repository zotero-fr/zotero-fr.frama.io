# Comment retrouver rapidement dans ma bibliothèque Zotero les documents pour lesquels il manque une date?

!!! danger "Contenu produit par le collectif Traduction-Zotero-fr"
    **Cette page a été entièrement écrite par le collectif Traduction-Zotero-fr et ne fait pas partie de la documentation officielle Zotero - dernière mise à jour : 2024-12-12**
    { data-search-exclude }

La [rubrique Rechercher > Recherche avancée](../searching.md/#recherche-avancée) présente de façon détaillée les différentes options du moteur de recherche de Zotero. Elle ne mentionne toutefois pas comment paramétrer une recherche pour repérer les documents pour lesquels la date de publication n’est pas renseignée. Or il importe de corriger cette omission, la date de publication fait partie des éléments de base d’une référence bibliographique.

Une combinaison de recherches enregistrées permet d'atteindre cet objectif.

Pour ces recherches enregistrées, veillez à cochez la case "Ne montrer que les documents de niveau supérieur" pour éviter que la recherche n’affiche toutes les notes et tous les fichiers joints, qui n’ont pas de date ni de champ équivalent à la date de publication.

1. Créez une recherche avancée avec les critères suivants, et enregistrez-la sous le nom "1000 ans" :

```
      Date — est dans les derniers — 1000 — années
```

![Fenêtre de création de la recherche enregistrée "1000 ans"](../images/kbfr_sans_date_rech01.png){class="img500px"}

2. Créez une nouvelle recherche avancée avec les critères suivants, et enregistrez-la sous le nom "sans date" ou tout autre intitulé significatif pour vous :

```
      Collection (de Zotero) — différent — 1000 ans
```

![Fenêtre de création de la recherche enregistrée "sans date"](../images/kbfr_sans_date_rech02.png){class="img500px"}

3. Si vous avez des documents antérieurs à 1020 dans votre bibliothèque, modifiez la valeur de la première recherche enregistrée, et portez-la à 2000 ans par exemple. Notez que la valeur maximale autorisée est 6732.

4. La recherche enregistrée "sans date" affiche tous les documents pour lesquels le champ date n'est pas renseigné.

![Affichage dans la bibliothèque Zotero des résultats répondant à la recherche enregistrée "sans date"](../images/kbfr_sans_date_rech03.png){class="img500px"}
