# Installer manuellement les extensions pour logiciel de traitement de texte de Zotero

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Manually Installing the Zotero Word Processor Plugin](https://www.zotero.org/support/word_processor_plugin_manual_installation) - dernière mise à jour de la traduction : 2024-11-21*
</div>

Les extensions de traitement de texte de Zotero s'installent automatiquement pour la plupart des utilisateurs. Si vous ne voyez pas la barre d'outils Zotero dans votre traitement de texte, vous devez [essayer de réinstaller l'extension](https://www.zotero.org/support/word_processor_plugin_troubleshooting#zotero_toolbar_doesn_t_appear) depuis le volet Citer → Traitements de texte des préférences Zotero. Si vous recevez une erreur ou ne voyez toujours pas l'extension après avoir essayé de la réinstaller depuis les paramètres, vous pouvez essayer les instructions d'installation manuelle ci-dessous.

Notez que si vous vous fiez à l'installation manuelle, vous risquez de rencontrer des problèmes plus tard en raison de l'absence de mise à jour de l'extension. Il est donc préférable de déterminer pourquoi l'installation automatique ne fonctionne pas (par exemple, un logiciel de sécurité bloquant l'installation ou [un emplacement incorrect du dossier de démarrage du traitement de texte](#localiser-votre-dossier-de-démarrage-word)) et de résoudre le problème sous-jacent.


## Word pour Windows

1.  Ouvrez le dossier d'installation de Zotero (`C:\Program Files (x86)\Zotero` pour Zotero 6 et `C:\Program Files\Zotero` pour Zotero 7).
2.  Dans le dossier d'installation, ouvrez `extensions\zoteroWinWordIntegration@zotero.org\install` (Zotero 6) ou `integration\word-for-windows` (Zotero 7), où vous trouverez une copie du fichier Zotero.dotm. 
    * Si le dossier est vide, le fichier a été supprimé d'une manière ou d'une autre - peut-être par un logiciel de sécurité - et vous devez réinstaller Zotero. 
    * Si le dossier est vide immédiatement après avoir résintallé Zotero, vous pouvez télécharger [Zotero.dotm pour Zotero 6](https://github.com/zotero/zotero-word-for-windows-integration/raw/6.0/install/Zotero.dotm) ou [Zotero.dotm pour Zotero 7](https://github.com/zotero/zotero-word-for-windows-integration/raw/main/install/Zotero.dotm), mais votre logiciel de sécurité peut supprimer de la même façon ce fichier téléchargé, et vous devrez le configurer pour qu'il n'effectue pas cette suppression.
    * Si vous voyez deux fichiers "Zotero" sans extension de fichier, votre ordinateur est réglé pour ne pas afficher les extensions de fichier. Vous pouvez déterminer lequel est Zotero.dotm en cliquant avec le bouton droit de la souris sur chaque fichier et en sélectionnant "Propriétés". L'un sera “Modèle Word 97-2003 (.dot)” et l'autre sera “Modèle Word prenant en charge les macros (.dotm)”.
3.  Trouvez votre dossier de démarrage Word et copiez le chemin dans le presse-papiers.
    * Dans le ruban de Word, cliquez sur l'onglet "Fichier", puis sur "Options" et enfin sur "Options avancées".
    * Sous "Général", cliquez sur "Emplacements des fichiers...". L'emplacement actuel du dossier de démarrage doit y être répertorié. 
     * Dans la plupart des cas, le chemin du dossier de démarrage doit être l'emplacement par défaut suivant : `%AppData%\Microsoft\Word\STARTUP`. Le chemin ne doit en aucune façon inclure "Zotero". Si c'est le cas, vous l'avez précédemment configuré de manière incorrecte, et vous devez alors le réinitialiser à l'emplacement par défaut.
    * Sélectionnez le chemin du dossier de démarrage et cliquez sur "Modifier", cliquez dans l'espace à droite du chemin dans la barre d'adresse en haut de la fenêtre, copiez le chemin complet dans le presse-papiers avec Ctrl+C, puis **cliquez sur Annuler** pour fermer la boîte de dialogue sans apporter de modifications.

4. Ouvrez une nouvelle fenêtre de votre explorateur de fichiers et collez le chemin du dossier de démarrage dans la barre d'adresse. Vous devez maintenant avoir deux dossiers ouverts : le dossier "install" contenant Zotero.dotm et le dossier de démarrage de Word.
4.  Copiez le fichier Zotero.dotm dans votre dossier de démarrage de Word. Veillez à copier le fichier plutôt que de le déplacer. Si vous le faites glisser, activez la touche Ctrl.
5.  Redémarrez Word pour commencer à utiliser l'extension.

## Word pour Mac 

1.  Dans Finder, appuyez sur Cmd+Maj+G et naviguez jusqu'à `/Applications/Zotero.app/Contents/Resources/extensions/zoteroMacWordIntegration@zotero.org/install` (Zotero 6) ou `/Applications/Zotero.app/Contents/Resources/integration/word-for-mac` (Zotero 7), où vous trouverez une copie du fichier Zotero.dotm. Si le dossier est vide, le fichier a été supprimé d'une manière ou d'une autre - peut-être par un logiciel de sécurité - et vous devez réinstaller Zotero.
2.  Trouvez votre dossier de démarrage Word en suivant les [instructions ci-dessous](#word-2016-et-2019-pour-mac). Vous devez maintenant avoir deux dossiers ouverts : le dossier de démarrage de Word et le dossier "install" contenant Zotero.dotm.
3.  Copiez le fichier Zotero.dotm dans votre dossier de démarrage de Word. (Veillez à copier le fichier plutôt que de le déplacer).
4.  Démarrez (ou redémarrez) Microsoft Word pour commencer à utiliser l'extension.

## LibreOffice

1.  Accédez aux fichiers d'application de Zotero.
    -   Mac : Dans Finder, appuyez sur Cmd+Shift+G et collez `/Applications/Zotero.app/Contents/Resources/extensions/zoteroOpenOfficeIntegration@zotero.org/install` (Zotero 6) ou `/Applications/Zotero.app/Contents/Resources/integration/Libreoffice` (Zotero 7)
    -   Windows : Ouvrez le dossier `C:\Program Files (x86)\Zotero\extensions\zoteroOpenOfficeIntegration@zotero.org\install` (Zotero 6) ou `C:\Program Files\Zotero\integration\Libreoffice` (Zotero 7)
    -   Linux : Allez dans le répertoire où Zotero est installé et ouvrez `extensions/zoteroOpenOfficeIntegration@zotero.org/install` (Zotero 6) ou `integration/libreoffice` (Zotero 7)
2.  Double-cliquez sur le fichier Zotero_OpenOffice_Integration.oxt pour l'installer. Sinon, dans LibreOffice, allez dans le menu Outils → Gestionnaire des extensions... , cliquez sur Ajouter et sélectionnez le fichier .oxt à partir du répertoire mentionné ci-dessus.

Si vous obtenez un message d'erreur, il y a un problème avec votre installation LibreOffice, et vous devez suivre [les étapes de dépannage](https://www.zotero.org/support/word_processor_plugin_troubleshooting#installation_error).

## Localiser votre dossier de démarrage Word

Remarque : dans des systèmes dans une langue autre que l'anglais ou dans certaines configurations personnalisées, ces emplacements peuvent être différents.

### Word 2007 ou supérieur pour Windows

L'emplacement par défaut du dossier de démarrage est le suivant : `%AppData%\Microsoft\Word\STARTUP`. 

Si les modifications que vous apportez au dossier de démarrage ne prennent pas effet, vous pouvez confirmer que Word n'est pas défini à un autre emplacement. Dans le ruban de Word, cliquez sur l'onglet "Fichier", puis sur "Options", et enfin sur "Options avancées". Sous "Général", cliquez sur "Emplacements des fichiers". Le dossier de démarrage doit y figurer. Sélectionnez-le et cliquez sur "Modifier". Dans la fenêtre qui s'ouvre, cliquez sur l'espace à droite du chemin dans la barre d'adresse en haut et copiez le chemin complet dans le presse-papiers en appuyant sur Ctrl+C. **Cliquez sur "Annuler"** pour fermer la boîte de dialogue sans effectuer de modifications. Vous pouvez ensuite ouvrir une nouvelle boîte de dialogue de l'explorateur de fichiers et coller le chemin d'accès dans la barre d'adresse pour ouvrir le dossier de démarrage. 

Notez que le chemin ne doit en aucune façon inclure "Zotero". Si c'est le cas, vous l'avez précédemment configuré de manière incorrecte, et vous devez alors le réinitialiser à l'emplacement par défaut.

### Word 2016 et 2019 pour Mac

L'emplacement par défaut est `~/Library/Group Containers/UBF8T346G9.Office/User Content/Startup/Word`.  (~/Library désigne le dossier Library de votre répertoire personnel.) Vous pouvez l'ouvrir à partir de Finder en appuyant sur Cmd+Shift+G et en copiant le chemin. Vous pouvez également y accéder dans Finder en maintenant la touche "Option" enfoncée, en cliquant sur le menu "Go", en sélectionnant "Library" (qui est caché par défaut), puis en suivant le reste du chemin.

Si les modifications apportées au dossier de démarrage ne sont pas prises en compte, vous pouvez vérifier que Word n'est pas à un autre emplacement. Dans Word, ouvrez le menu "Word" en haut à gauche de l'écran et sélectionnez "Préférences". Cliquez sur "Emplacements des fichiers" sous "Paramètres personnels" et cliquez sur "Démarrage" en bas de la liste.

En général, aucun emplacement ne doit être indiqué, ce qui amène Word à utiliser l'emplacement par défaut. Si un autre emplacement est indiqué (par exemple, `/Applications/Microsoft Office 2011/Office/Startup/Word`, à partir d'une version antérieure de Word), effacer le paramètre et laisser Word utiliser l'emplacement par défaut peut résoudre les problèmes d'installation et permettre à Zotero d'installer l'extension automatiquement à l'avenir.

Notez que le chemin ne doit en aucun cas inclure "Zotero", et si c'est le cas c'est que vous l'avez configuré de manière incorrecte. Si c'est le cas, vous devez réinitialiser le chemin pour qu'il soit vide et que l'emplacement par défaut soit utilisé. 

### Word 2011 pour Mac

L'emplacement par défaut du dossier de démarrage est `/Applications/Microsoft Office 2011/Office/Startup/Word`. Vous pouvez l'ouvrir à partir de Finder en appuyant sur Cmd+Shift+G et en copiant le chemin d'accès ou en y accédant par navigation.

Si les modifications que vous apportez au dossier de démarrage ne prennent pas effet, vous pouvez confirmer que Word n'est pas localisé à un autre emplacement. Dans Word, ouvrez le menu "Word" en haut à gauche de l'écran et sélectionnez "Préférences". Cliquez sur "Emplacements des fichiers" sous "Paramètres personnels" et cliquez sur "Démarrage" en bas de la liste.

