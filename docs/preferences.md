# Les paramètres de Zotero

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Preferences](https://www.zotero.org/support/preferences) - dernière mise à jour de la traduction : 2024-12-10*
</div>

## La fenêtre de paramétrage

De nombreuses fonctions de Zotero peuvent être personnalisées via la fenêtre de paramétrage de Zotero. Pour ouvrir les paramètres, cliquez sur le menu "Édition -&gt; Paramètres" (sur Windows ou Linux) ou "Zotero -&gt; Paramètres" (sur Mac). Vous pouvez aussi utiliser le raccourci clavier `Ctrl/Cmd`-`,`.

![Présente les rubriques de la fenêtre de paramétrage](./images/preferences_tabs_FR.png)

La fenêtre de paramétrage est divisée en différentes rubriques.

-   **[Général](./general.md)** - Ajuster l'apparence, les paramètres d'importation et d'autres fonctions générales.
-   **[Synchronisation](./preferences_sync.md)** - Configurer la synchronisation des données et des fichiers.
-   **[Exportation](./export.md)** - Définir les paramètres par défaut pour la génération des bibliographies et des citations.
-   **[Citer](./cite.md)** - Ajouter, retirer, modifier et prévisualiser les styles bibliographiques, et installer les extensions de traitement de texte.
-   **[Avancé](./advanced.md)** - Localisation des données Zotero, indexation du texte intégral, et autres paramètres avancés.

## Préférences cachées

En plus des paramètres affichés dans la fenêtre de paramétrage de Zotero, il existe un certain nombre de [préférences cachées (page en anglais)](https://www.zotero.org/support/preferences/hidden_preferences) qui ne peuvent être modifiées que par le biais de l'Éditeur de configuration, accessible depuis la rubrique "Avancé" de la fenêtre de paramétrage.
