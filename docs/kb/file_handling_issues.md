# Problèmes de gestion des fichiers

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [File Handling Issues](https://www.zotero.org/support/kb/file_handling_issues) - dernière mise à jour de la traduction : 2023-01-18*
</div>

## Comportement inattendu (action erronée, charabia ("%PDF..."), etc.) lors de l'ouverture de fichiers depuis Zotero

La suppression de mimeTypes.rdf de votre [répertoire de profil](./profile_directory.md) et le redémarrage de Zotero sont généralement nécessaires pour résoudre les problèmes de gestion de fichiers. mimeTypes.rdf stocke les associations de gestion de fichiers et sera recréé automatiquement au prochain redémarrage de Zotero.

## Les PDFs s'ouvrent dans une mauvaise application sur les systèmes Linux

Bien que Zotero soit configuré pour utiliser le lecteur PDF par défaut du système, la gestion des fichiers sous Linux peut être affectée par de nombreux paramètres. Si vous trouvez que les PDFs s'ouvrent dans, disons, Gimp même si vous avez configuré le gestionnaire de fichier pour être Okular dans Dolphin/Nautilus/etc., la solution la plus simple est de choisir un lecteur PDF spécifique dans le panneau "Générales" des préférences de Zotero.

Si vous souhaitez conserver le paramètre "System Default", vous pouvez essayer ce qui suit.

Ouvrez le fichier

```
~/.local/share/applications/defaults.list
```

dans un éditeur de texte et recherchez la ligne "application/pdf=...". Changez-la en

```
application/pdf=kde4-okularApplication_pdf.desktop
```

Si le fichier n'existe pas, créez-le et entrez le contenu suivant :

```
[Default Applications]
application/pdf=kde4-okularApplication_pdf.desktop
```

Remarque : sur certains systèmes, il se peut que vous deviez utiliser à la place.

```
application/pdf=kde-okularApplication_pdf.desktop
```

Pour les autres lecteurs de PDF, vous pouvez utiliser l'une des options suivantes :

```
application/pdf=evince.desktop
```

```
application/pdf=acroread.desktop
```

```
application/pdf=Foxit-Reader.desktop
```
