#  Comment exporter ma bibliothèque Zotero ?


<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How do I export my Zotero library?](https://www.zotero.org/support/kb/exporting) - dernière mise à jour de la traduction : 2022-11-27*
</div>

Pour exporter une bibliothèque entière, cliquez dessus avec le bouton droit de la souris dans le volet des collections Zotero et choisissez "Exporter la bibliothèque...", ou sélectionnez "Exporter la bibliothèque..." dans le menu "Fichier". Pour exporter une collection individuelle, faites un clic droit dessus et choisissez "Exporter la collection...". Pour exporter des documents spécifiques, sélectionnez-les dans la liste des documents, faites un clic droit et choisissez "Exporter les documents...".

Lorsque vous partagez des documents avec un autre utilisateur de Zotero, sélectionnez "Zotero RDF with files and notes" pour un transfert le plus complet.

**Remarque** : l'importation/exportation n'est pas recommandée pour [transférer des bibliothèques Zotero entières entre ordinateurs](./transferring_a_library.md) et ne remplace pas non plus la collaboration continue à l'aide des [groupes Zotero](../groups.md). Les documents réimportés ont de nouvelles dates d'ajout et de modification, ne sont plus liés aux citations dans les documents de traitement de texte existants, et peuvent présenter des modifications mineures dans un petit nombre de champs. 
