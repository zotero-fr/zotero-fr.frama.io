# Connecteur Zotero et Safari

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Zotero Connector and Safari](https://www.zotero.org/support/kb/safari_compatibility) - dernière mise à jour de la traduction : 2025-01-23*
</div>

## Installation

Le connecteur Zotero pour Safari est inclus dans l'application de bureau Zotero. (Les versions actuelles de Safari ne permettent pas l'installation directe d'extensions de navigateur comme les autres navigateurs). Après avoir ouvert Zotero pour la première fois, vous pouvez activer le connecteur Zotero dans le volet "Extensions" des préférences de Safari (menu "Safari" → "Réglages..." → "Extensions", **et non** menu "Safari" → "Extensions..." ).

Le connecteur Zotero pour Safari nécessite macOS 11 Big Sur ou une version ultérieure.

Vous utilisez un iPhone ou un iPad ? Vous pouvez enregistrer dans l’[application Zotero pour iOS](https://apps.apple.com/us/app/zotero/id1513554812) en utilisant les options de partage de Safari et des autres navigateurs web.

![Le connecteur Zotero dans le gestionnaire d'extensions de Safari](../images/safari-compatibility_FR.png)

## Le bouton d'enregistrement n'apparaît pas ? Le bouton d'enregistrement clignote ?

Si vous constatez que l'extension a disparu de la barre d'outils de Safari ou qu'elle apparaît et réapparaît rapidement, essayez de fermer Safari, de supprimer l'application Zotero des Applications et de télécharger à nouveau Zotero depuis la [page de téléchargement](https://www.zotero.org/download). (Vos données Zotero ne seront pas affectées.) Il peut également être nécessaire de redémarrer votre ordinateur entre la suppression de l'application et sa réinstallation.

Nous pensons que les causes potentielles de ce problème ont été corrigées dans Zotero 7. Si vous continuez à rencontrer ce problème, faites-le nous savoir dans les Forums Zotero. 

## Limitations

En raison des limitations techniques du système d'extensions de Safari, certaines fonctionnalités disponibles dans Firefox, Chrome et Edge ne le sont pas dans Safari :

* Redirection automatique vers un proxy
* Importation automatique de RIS/BibTeX
* Installation automatique de CSL

**Autres différences :**

* Les PDF sous accès contrôlé (*gated PDF*) peuvent ne pas être enregistrés sur certains sites (par exemple ScienceDirect).
* Il n'est pas possible de cliquer avec le bouton droit de la souris sur le bouton de la barre d'outils pour accéder aux convertisseurs secondaires. À la place, cliquez avec le bouton droit de la souris sur la page elle-même.
