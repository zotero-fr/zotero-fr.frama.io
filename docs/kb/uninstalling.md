# Comment désinstaller Zotero ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How do I uninstall Zotero?](https://www.zotero.org/support/kb/uninstalling) - dernière mise à jour de la traduction : 2023-03-30*
</div>

Vous pouvez désinstaller Zotero comme n'importe quel programme sur votre ordinateur. Le [répertoire de données Zotero](../zotero_data.md) n'est pas supprimé lorsque vous désinstallez Zotero.

Vous pouvez désinstaller le connecteur Zotero à partir du volet "Extensions" de votre navigateur.

Pour désinstaller les extensions de logiciel de traitement de texte Zotero, consultez [ces instructions](./uninstalling_the_word_processor_plugin.md).