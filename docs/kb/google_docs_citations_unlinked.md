
# Pourquoi Zotero ne détecte-t-il pas mes citations existantes dans Google Docs?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Why isn’t Zotero detecting my existing Google Docs citations?](https://www.zotero.org/support/kb/google_docs_citations_unlinked) - dernière mise à jour de la traduction : 2023-02-28*
</div>

L'intégration de Google Docs utilise un mécanisme de stockage des citations différent de celui utilisé par les extensions pour Word et LibreOffice, et les liens des citations peuvent être plus facilement brisés accidentellement. La raison la plus fréquente est que des collaborateurs modifient le document sans avoir installé le connecteur Zotero. Pour que les citations Zotero restent liées, toutes les personnes qui modifient le Google Doc doivent [installer le connecteur Zotero](https://www.zotero.org/download/) dans leur navigateur. L'application Zotero elle-même n'est requise que pour l'insertion et la modification des citations. Les liens des citations peuvent également être brisés [si vous faites glisser des citations dans le Google Doc](../google_docs.md#limitations) au lieu de les couper et de les coller.

Pour restaurer des citations non liées, vous avez deux possibilités.

1. Utilisez l'historique des versions de Google Docs et rétablissez une version antérieure du document avant la rupture des liens des citations. L'historique des versions se trouve sous Google Docs → File → Version History → "See version history". Notez que l'avertissement concernant les citations non liées s'affichera lors de l'opération suivante, c'est-à-dire *après* celle qui a causé le problème. Aussi restaurer à une version antérieure qui n'affiche pas immédiatement les citations comme non liées n'est pas suffisant - vous devez appuyer sur "Refresh" dans le menu Zotero et confirmer que l'avertissement ne réapparaît pas. Si l'avertissement réapparaît, revenez à une version antérieure.

2. Utilisez l’extension Zotero pour réinsérer les citations manquantes non liées.

Si tous vos collaborateurs ont installé le connecteur Zotero mais que vous constatez toujours que des citations ne sont plus liées, essayez d'identifier les actions qui ont conduit à la rupture des liens via l'historique des versions de Google Docs et publiez les détails sur [les forums Zotero](https://www.zotero.org/forum). Encore une fois, assurez-vous d'appuyer sur "Refresh" après la restauration pour confirmer que les citations ne sont pas déjà dissociées dans cette version.

Pour des informations concernant les autres extensions de traitement de texte, voir [Citations existantes non détectées](./existing_citations_not_detected.md).
