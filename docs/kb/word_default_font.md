# Les références apparaissent dans la mauvaise police dans Word/LibreOffice

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [References appear in the wrong font in Word/LibreOffice](https://www.zotero.org/support/kb/word_default_font) - dernière mise à jour de la traduction : 2025-01-23*
</div>

Les extensions pour logiciel de traitement de texte appliquent le style "Style de paragraphe par défaut" (LibreOffice) ou "Normal"/"Standard" (Word) aux citations générées et au paragraphe dans lequel elles sont insérées. La bibliographie est rendue dans un style différent - "Bibliographie" (Word) ou "Bibliographie1" (LibreOffice). Par conséquent, l'insertion d'une citation avec Zotero peut supprimer la mise en forme (les retraits, l'interligne, la taille de la police, etc.) d'un paragraphe entier et la citation peut apparaître dans une police ou un format non souhaité. Vous pouvez corriger la mise en forme en ajustant le style "Style de paragraphe par défaut/Normal/Standard" (pour les citations) et le style "Bibliographie/Bibliographie1" (pour la bibliographie) dans votre traitement de texte.

## Solutions

### Word pour Windows 

Placez votre curseur dans une citation ou une bibliographie Zotero. Ensuite, cliquez sur le bouton déroulant dans le coin inférieur droit du sélecteur rapide de style de l'onglet "Accueil" et choisissez "Modifier le style". Apportez les modifications nécessaires à la police et à la mise en forme des paragraphes pour les styles "Normal" ou "Bibliographie" et cliquez sur "OK". Vous pouvez également modifier la mise en en forme de citations individuelles en utilisant les options des groupes "Police" et "Paragraphe" de l'onglet "Accueil". Voir [un guide avec des captures d'écran (en anglais)](http://www.lostintechnology.com/how-to/how-to-change-the-default-settings-in-microsoft-word-2007).

### Word pour Mac

Placez votre curseur dans une citation ou une bibliographie Zotero. Cliquez ensuite sur le bouton "Volet des styles" à l'extrémité de l'onglet "Accueil". Cliquez sur le titre "Style actuel" en haut du volet et choisissez "Modifier le style". Apportez les modifications nécessaires à la police et à la mise en forme des paragraphes pour les styles "Normal" ou "Bibliographie" et cliquez sur "OK". Vous pouvez également modifier la mise en forme des citations individuelles en utilisant les options "Police" et "Paragraphe" du menu "Format". Voir [un guide avec des captures d'écran (en anglais)](https://support.office.com/en-us/article/Customize-styles-in-Word-for-Mac-1ef7d8e1-1506-4b21-9e81-adc5f698f86a).

### LibreOffice

Dans LibreOffice, ouvrez le gestionnaire de styles dans "Styles" -> "Gérer les styles" ou en appuyant sur F11. Faites un clic droit sur "Style de paragraphe par défaut" ou "Bibliographie1", sélectionnez "Editer le style..." et apportez les modifications souhaitées à ce style.
