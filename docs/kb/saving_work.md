# Comment puis-je enregistrer mon travail?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How do I save my work?](https://www.zotero.org/support/kb/saving_work) - dernière mise à jour de la traduction : 2023-04-06*
</div>

Zotero enregistre automatiquement tout ce que vous saisissez.
