# Pourquoi les pièces jointes portent-elles des noms tels que "PDF" ou "Version acceptée" dans la liste des documents, au lieu de leur nom de fichier  ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [File Renaming](https://www.zotero.org/support/kb/attachment_title_vs_filename) - dernière mise à jour de la traduction : 2025-02-09*
</div>

Les pièces jointes ont deux noms distincts : le titre de la pièce jointe affiché dans la liste des documents et le nom du fichier sur le disque.

Zotero a toujours renommé automatiquement le fichier sur le disque à partir des métadonnées de son document parent, telles que le titre et les auteurs. Comme la ligne du document parent affiche déjà ces métadonnées, Zotero n'affiche pas le nom du fichier directement dans la liste des documents. Il utilise plutôt des titres de pièces jointes plus simples tels que "PDF" ou "Ebook" pour le premier fichier d'un type donné, ou il inclut des informations supplémentaires concernant la source du fichier (par exemple, "ScienceDirect Full Text PDF" pour un fichier enregistré depuis ScienceDirect, ou "Version acceptée" ou "Version soumise" pour les [fichiers en libre accès](https://zotero.hypotheses.org/2130)). Ces titres distincts évitent d'encombrer la liste des documents avec des métadonnées en double, et évitent que les documents parents soient inutilement développés lors de la recherche par titre ou par auteur.

Les fichiers ultérieurs ajoutés à un document à partir du système de fichiers voient toujours leur titre construit à partir de leur nom de fichier (sans l'extension du fichier), car il s'agit probablement de fichiers supplémentaires, et le nom du fichier peut alors être informatif.

Vous pouvez voir et modifier le titre et le nom du fichier en cliquant sur la pièce jointe et en regardant dans le volet de droite.

## Gestion des titres de pièces jointes dans Zotero 7

Zotero 7 apporte quelques modifications à la gestion des titres des pièces jointes.

1. Avant Zotero 7, si vous lanciez manuellement la focntionnalité "Renommer le fichier à partir des métadonnées du parent", le titre de la pièce jointe était remplacé par le nom du fichier. Il s'agissait d'un bogue, qui laissait penser que les fichiers n'étaient pas automatiquement renommés et qu'il était nécessaire de lancer "Renommer le fichier à partir des métadonnées du parent" sur chaque nouvelle pièce jointe. Le titre n'est plus modifié et les titres restent "PDF", "ScienceDirect Full Text PDF" ou tout autre titre défini précédemment.
2. Lors du glissement d'un fichier depuis le système de fichiers ou lors de la création d'un document parent, Zotero définit désormais le titre de la première pièce jointe d'un type donné à "PDF", "EPUB", etc., au lieu de définir le titre en fonction du nom de fichier.

Les personnes qui lançaient inutilement "Renommer le fichier à partir des métadonnées du parent" sur chaque pièce jointe ou qui utilisaient l'extension ZotFile (qui définit également le titre en fonction du nom de fichier) peuvent être habituées à voir les noms de fichiers dans la liste des documents, mais nous les encourageons à essayer le nouveau comportement.

Si dans un souci de cohérence vous souhaitez convertir les pièces jointes existantes pour utiliser les titres "PDF", vous pouvez sélectionner tous les documents dans la liste des documents, aller dans Outils → Développeur → Run JavaScript, et exécuter le code suivant.

```
var items = ZoteroPane.getSelectedItems() ;
for (let item of items) {
    if (!item.isRegularItem()) continue ;
    let attachment = await item.getBestAttachment() ;
    if (!attachment) continue ;
    let title = attachment.getField('title') ;
    if (title.endsWith('.pdf')) {
        attachment.setField('title', 'PDF') ;
        await attachment.saveTx() ;
    }
}
```

Cette méthode passe en revue la pièce jointe principale de chaque document sélectionné et convertit le titre de toutes celles qui se terminent par « .pdf » en « PDF ».

Vous devez d'abord tester avec un petit nombre de documents sélectionnés pour vous assurer que le résultat est conforme à vos attentes.
