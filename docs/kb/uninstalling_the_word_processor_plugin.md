#  Comment désinstaller les extensions pour logiciel de traitement de texte de Zotero ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How do I uninstall the Zotero word processor plugins?](https://www.zotero.org/support/kb/uninstalling_the_word_processor_plugin) - dernière mise à jour de la traduction : 2023-01-13*
</div>

## Word

Vous pouvez désinstaller l’extension Word en supprimant le fichier Zotero.dotm de [votre dossier de démarrage  Word](../word_processor_plugin_manual_installation.md/#localiser-votre-dossier-de-demarrage-word) et en redémarrant Word.

## LibreOffice

Vous pouvez désinstaller l’extension LibreOffice depuis le menu "Outils" → "Gestionnaire d'extensions...", en sélectionnant "Intégration Zotero LibreOffice", puis en cliquant sur "Supprimer".

## Google Docs

L’extension Google Docs fait partie du connecteur Zotero. Si vous n'utilisez plus Zotero, vous pouvez désinstaller le connecteur Zotero dans le volet "Extensions" de votre navigateur. Si vous souhaitez continuer à utiliser le connecteur Zotero mais ne voulez pas que le menu Zotero ou le bouton de la barre d'outils soit intégré à Google Docs, vous pouvez désactiver l'intégration de Google Docs dans le volet "Advanced" des préférences du connecteur Zotero.
