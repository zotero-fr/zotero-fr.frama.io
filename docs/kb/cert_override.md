# Outrepasser les erreurs de certificat dans Zotero

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Overriding Security Certificate Errors in Zotero](https://www.zotero.org/support/kb/cert_override) - dernière mise à jour de la traduction : 2024-10-05*
</div>

**Note** : Ces instructions doivent être utilisées uniquement avec un logiciel de sécurité qui intercepte/scanne les connexions HTTPS, un serveur WebDAV avec un certificat auto-signé, ou un réseau institutionnel qui surveille le trafic chiffré à l'aide d'une autorité de certification (CA) racine personnalisée. Vous ne devez jamais ignorer les erreurs de certificat [si vous n'en connaissez pas les conséquences](./ssl_certificate_error.md). En cas de doute, veuillez contacter votre administrateur réseau ou votre fournisseur d'accès à Internet.

## Certificat auto-signé

Zotero ne fournit pas actuellement de moyen graphique pour mettre sur liste blanche les certificats auto-signés, vous devrez donc copier les fichiers à partir d'une installation Firefox fonctionnelle.

Si vous utilisez un serveur WebDAV avec un certificat auto-signé, vous pouvez ouvrir l'URL WebDAV dans Firefox, accepter le certificat, puis copier le fichier `cert_override.txt` depuis le [répertoire de profil de Firefox](http://support.mozilla.com/kb/Profiles) vers le [répertoire de profil de Zotero](./profile_directory.md).

### Zotero 6

Zotero 6 attend un fichier `cert_override.txt` créé par Firefox 60 ESR, avec une ligne de cette forme :

`192.168.xxx.xxx:1234    OID.2.16…    1D:E4:07:…    U    AAAA…`

Si vous créez un fichier override avec une version plus récente de Firefox, votre fichier `cert_override.txt` peut contenir une ligne avec un deux-points après le numéro de port (1234 dans cet exemple) et il peut manquer une ou plusieurs lettres avant "AAAA" ("U" dans l’exemple ci-dessus) :

`192.168.xxx.xxx:1234:    OID.2.16…    1D:E4:07:…    AAAA…`

Pour utiliser un tel fichier dans Zotero 6, il suffit de supprimer les deux points après le numéro de port et d'ajouter un "U" (pour untrusted certificate) avant "AAAA". Pour autoriser une incohérence de nom de domaine, ajouter "M".

### Zotero 7 

Zotero 7 peut lire un fichier cert_override.txt depuis Firefox 115 ESR. Un fichier provenant d'une version ultérieure de Firefox pourra fonctionner ou non.

## Autorité de certification personnalisée

Si vous ou votre organisation utilisez une autorité de certification personnalisée, ce qui peut être le cas lorsque vous utilisez un logiciel de sécurité ou que vous vous connectez via un serveur proxy, Zotero peut avoir besoin d'être configuré pour accepter l'autorité de certification personnalisée :

* **Windows/Mac** : Zotero 7 utilise automatiquement le magasin de certificats des autorités de certifications racines du système, ce qui, dans la plupart des cas, devrait lui permettre de fonctionner automatiquement comme les autres navigateurs du système.

* **Linux** : Zotero est basé sur Firefox et utilise le même mécanisme de certificat, aussi vous ou votre service informatique devrez configurer Firefox pour l'autorité de certification personnalisée dans un nouveau profil Firefox 115 ESR, puis copier les fichiers `cert9.db`, `key4.db` et `pkcs11.txt` à partir du [Répertoire de profils Firefox](http://support.mozilla.com/kb/Profiles) vers le [répertoire de profil Zotero](./profile_directory.md).