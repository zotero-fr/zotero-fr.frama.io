# Puis-je empêcher la boîte de dialogue "Ajouter une citation" des extensions pour logiciel de traitement de texte de se déplacer derrière la fenêtre du traitement de texte ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Can I prevent the "Add Citation" dialog from the word processor plugins from moving behind the word processor window?](https://www.zotero.org/support/kb/addcitationdialog_raised) - dernière mise à jour de la traduction : 2022-09-28*
</div>

Oui, en modifiant la [préférence cachée](https://www.zotero.org/support/preferences/hidden_preferences#word_processor_plugin) extensions.zotero.integration.keepAddCitationDialogRaised.


