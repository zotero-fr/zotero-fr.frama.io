# Je dois utiliser le style Chicago. Laquelle des trois versions fournies avec Zotero dois-je utiliser ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [I need to use Chicago style. Which of the three versions that come with Zotero should I use?](https://www.zotero.org/support/kb/chicago_style_versions) - dernière mise à jour de la traduction : 2023-03-22*
</div>

Zotero est livré avec trois variantes du style Chicago (des exemples mis en forme pour chaque style sont présentés ci-dessous). Le format auteur-date (*author-date*) est le plus populaire dans les sciences physiques, naturelles et sociales, tandis que les chercheurs dans les domaines littéraires, historiques et artistiques utilisent principalement des styles basés sur les notes de bas de page. Dans les variantes basées sur les notes, les notes peuvent soit être entièrement explicites et accompagnées ou non d'une bibliographie (*full note*), soit servir de renvoi aux références en bibliographie (*note*).

* Manuel de style de Chicago, 17e édition (*author-date*)
    * **Citation abrégée dans le texte** : (Adams 2002, 12)
    * **Référence en bibliographie** : Adams, Douglas. 2002. *The Ultimate Hitchhiker’s Guide to the Galaxy*. New York : Del Rey.
* Manuel de style de Chicago, 17e édition (*full note*)
    * **Note de bas de page** : Douglas Adams, *The Ultimate Hitchhiker's Guide to the Galaxy* (New York : Del Rey, 2002), 12.
    * **Référence en bibliographie** : Adams, Douglas. *The Ultimate Hitchhiker's Guide to the Galaxy*. New York : Del Rey, 2002.
* Manuel de style de Chicago, 17e édition (*note*)
    * **Note de bas de page** : Adams, *The Ultimate Hitchhiker's Guide to the Galaxy*, 12.
    * **Référence en bibliographie** : Adams, Douglas. *The Ultimate Hitchhiker's Guide to the Galaxy*. New York : Del Rey, 2002.
