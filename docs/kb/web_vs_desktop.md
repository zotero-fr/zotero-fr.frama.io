# Est-ce que la bibliothèque en ligne est identique à l’application de bureau Zotero ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Is the Zotero web library the same as the Zotero desktop app?](https://www.zotero.org/support/kb/web_vs_desktop) - dernière mise à jour de la traduction : 2024-08-06*
</div>

L’[application de bureau Zotero](https://www.zotero.org/download) est le principal moyen d'utiliser Zotero et offre des fonctionnalités complètes. Lorsque les gens parlent de "Zotero", ils font presque toujours référence à l’application de bureau.

La [bibliothèque en ligne de Zotero](https://www.zotero.org/mylibrary) est un outil complémentaire qui vous permet d'accéder à vos données Zotero lorsque vous n'êtes pas sur votre ordinateur principal ou lorsque vous utilisez une plateforme qui ne peut pas exécuter Zotero (ordinateurs institutionnels verrouillés, certains Chromebooks, etc.) La bibliothèque en ligne vous permet également de partager publiquement une bibliothèque Zotero avec des personnes qui n'utilisent pas Zotero.

Une application de bureau offre beaucoup plus de fonctionnalités qu'un site web. Voici une liste non exhaustive des fonctionnalités que seule l'application de bureau Zotero offre.

* Une base de données locale entièrement sous votre contrôle, avec la possibilité de synchroniser vos données.
* Un accès rapide et hors ligne à toutes vos données - alors que la bibliothèque en ligne ne peut charger qu'un sous-ensemble de vos données à la fois, l'application de bureau vous permet d'accéder à toutes vos données et de les modifier beaucoup plus rapidement.
* Des extensions pour Word, LibreOffice et Google Docs qui vous permettent d'insérer des citations et des bibliographies directement depuis Zotero et de les mettre à jour automatiquement.
* Une meilleure expérience lors de l'enregistrement de documents à partir du connecteur Zotero (+ [d'autres fonctionnalités](../connector.md)).
* La possibilité de travailler avec un grand nombre de références - certaines opérations, comme l'exportation et la génération de bibliographies, sont limitées à 100 unités dans la bibliothèque en ligne pour des raisons techniques.
* Des exports plus complets avec des options permettant d'inclure des fichiers et des notes dans certains formats.
* De vraies fenêtres avec lesquelles vous pouvez interagir via Cmd-Tab/Alt-Tab, que vous pouvez redimensionner, etc. - pas seulement une interface dans un seul onglet du navigateur.
* Accès au système de fichiers local - par exemple, ajout de PDF directement à partir de vos documents ou création de liens vers des fichiers sur le disque local.
* Récupération automatique des métadonnées des PDFs.
* Fonctionnalité "Trouver un PDF disponible" pour localiser une version en libre accès d'un fichier.
* Importation de références à partir d'un fichier ou d'autres logiciels de gestion bibliographique.
* Indexation du texte intégral des PDF (bientôt disponible dans la bibliothèque en ligne).
* Recherche avancée et recherches enregistrées.
* Notifications pour les articles rétractés.
* Collection "Mes Publications" pour partager publiquement votre travail.
* Éditeur de style CSL.
* Styles de citation personnalisés.
* Collection pour les documents non classés (bientôt disponible dans la bibliothèque en ligne).
* Collection pour les références en doublon et fusion des références.
* Possibilité d'associer des documents avec un lien "Connexe".
* Une interface utilisateur qui s'adapte au système d'exploitation que vous utilisez.
* Un système d’extensions sans limitations, qui permet d'ajouter de nombreuses fonctionnalités et de personnaliser davantage l'application.
